from datetime import datetime
from unittest.mock import MagicMock
from src.API import fetchWeatherAPI, Insert_City, dt2sec, unix2dt
import pytest

@pytest.mark.parametrize(
    "city, code",
    [
        ("Hanoi", 200),
        ("Hochiminh", '404')
    ]
)

def test_fetchAPI(city, code):
    result = fetchWeatherAPI(city)
    assert(result['cod'] == code)


@pytest.mark.parametrize(
    "datetime, seconds",
    [
        (datetime(2016, 5, 20, 19, 10, 0), 69000),
        (datetime(2016, 5, 20, 6, 10, 30), 22230),
        (datetime(2016, 5, 20, 0, 0, 0), 0),
        (datetime(2016, 5, 20, 23, 59, 59), 86399),
        (datetime(2016, 5, 20, 0, 10, 0), 600),
        (datetime(2016, 5, 20, 10, 0, 0), 36000)
    ]
)

def test_dt2sec(datetime, seconds):
    assert(dt2sec(datetime) == seconds)


@pytest.mark.parametrize(
    "unix_code, timezone, datetime",
    [
        (1653517797, 25200, datetime(2022, 5, 26, 5, 29, 57)),
        (1653563462, 25200, datetime(2022, 5, 26, 18, 11, 2)),
        (1653533669, None, datetime(2022, 5, 26, 9, 54, 29)),
        (1653941593, 32400, datetime(2022, 5, 31, 5, 13, 13)),
        (1653993992, 32400, datetime(2022, 5, 31, 19, 46, 32)),
    ]
)

def test_unix2dt(unix_code, timezone, datetime):
    assert(unix2dt(unix_code, timezone) == datetime)
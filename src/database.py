from conn import db 

def Create_Database_and_Tables(db):
	sql_query = [
		''' 
		IF DB_ID('WeatherAPI') IS NULL
		BEGIN
			CREATE DATABASE WeatherAPI
		END;''',
		'''
		USE WeatherAPI;
					
		IF OBJECT_ID('WeatherCondition', 'U') IS NULL
		BEGIN
			CREATE TABLE WeatherCondition ( 
				ConditionId int PRIMARY KEY ,
				Main varchar(255) NOT NULL,
				Discription varchar(255) NOT NULL,
				Icon varchar(255) NOT NULL)
		END;
		IF OBJECT_ID('City', 'U') IS NULL
		BEGIN
			CREATE TABLE City ( 
				CityId int PRIMARY KEY ,
				CityName varchar(255) NOT NULL,
				Latitude decimal(8,6) NOT NULL,
				Longtitude decimal(9,6) NOT NULL)
		END;
		IF OBJECT_ID('Station', 'U') IS NULL
		BEGIN
			CREATE TABLE Station ( 
				StationId int PRIMARY KEY ,
				Type int NOT NULL,
				Country varchar(255) NOT NULL,
				TimeZone varchar(255) NOT NULL)
		END;		
		IF OBJECT_ID('Record', 'U') IS NULL
		BEGIN
			CREATE TABLE Record ( 
				Id int IDENTITY(1,1) PRIMARY KEY ,
				Temperature float NOT NULL,
				TempMin float NOT NULL,
				TempMax float NOT NULL,
				Pressure float NOT NULL,
				Humidity float NOT NULL,
				SeaLevel float NOT NULL,
				GroundLevel float NOT NULL,
				Visibility int NOT NULL,
				WindSpeed float NOT NULL,
				WindDirection float NOT NULL,
				WindGust float NOT NULL,
				Cloudiness float NOT NULL,
				RainVolumeLastHour float NOT NULL,
				SnowVolumeLastHour float NOT NULL,
				Sunrise int NOT NULL,
				Sunset int NOT NULL,
				TimeRecord datetime NOT NULL,
				ConditionId int NOT NULL FOREIGN KEY REFERENCES WeatherCondition(ConditionId),
				CityId int NOT NULL FOREIGN KEY REFERENCES City(CityId),
				StationId int NOT NULL FOREIGN KEY REFERENCES Station(StationId))
		END;''']
	
	for query in sql_query:
		db.execute(query)

Create_Database_and_Tables(db)
print('Database and Tables Created')
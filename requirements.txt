coverage==6.4
numpy==1.21.6
pytz==2022.1
requests==2.27.1
pytest==7.1.2
pytest-cov==3.0.0
schedule==1.1.0

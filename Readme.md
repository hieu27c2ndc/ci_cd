# Weather API
- This API will get weather info in one of these cities at time: "Hanoi", "Seoul", "Tokyo", "Bangkok", "California", "Saigon", "London", "Paris"

# Requirement
- You must install below applications and python --version 3.7 or above to run the project

## SQL Sever and SSMS
- You can download SQL Sever at [https://www.microsoft.com/en-us/sql-server/sql-server-downloads](https://www.microsoft.com/en-us/sql-server/sql-server-downloads)

- You can download SSMS at [https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver16](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver16)

- You can follow this [link](https://blogchiasekienthuc.com/windows-server/cai-dat-microsoft-sql-server-tren-windows-10.html) to install SQL Sever and SSMS

## Python
- You can download Python at [https://www.python.org/downloads/](https://www.python.org/downloads/)
- Choose your version and follow this [link](https://quantrimang.com/cach-cai-dat-python-tren-windows-macos-linux-140625) to install



# Set up
- Open conn.py and change SERVER value to your SERVER NAME which use to connect with SQL SEVER in SSMS   
- Open cmd and run these commands to install needed python libraries. You can skip which library already installed:
    - pip install pyodbc
    - pip install datetime
    - pip install json
    - pip install request
    - pip install random
    - pip install numpy
    - pip install pytz
    - pip install schedule

# Run
- Befor run please make sure that you didnt have any database name WeatherAPI in SSMS
- Open cmd and run these commands:
    - Run this if your project folder located at another disc: Disc_Name_Contain_Project: -- example: E:
    - cd path_of_src_folder -- example: cd E:\ETL\src
    - py database.py -- this will help you create needed database and table in SSMS
    - py API.py

# Backup a database
- Befor backup you must move .bak file to Backup folder of SSMS which usually located at 'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\Backup'
- Use .bak file in database folder of project and follow this [link](https://docs.microsoft.com/en-us/sql/relational-databases/backup-restore/quickstart-backup-restore-database?view=sql-server-ver16) to backup a database

# Explain field in table
## City
- CityId    : City ID
- CityName  : City name
- Latitude  : City geo location, latitude
- Longtitude: City geo location, longtitude

## WeatherCondition
- ConditionId   : Weather condition ID
- Main          : Group of weather (Rain, Snow, Extreme etc.)
- Discription   : Weather condition within the group.
- Icon          : Weather icon (get icon image below)

## Station
- StationId : Station ID
- Country   : Country code (GB, JP etc.)
- Timezone  : Timezone of country

## Record
- Id                : Record ID
- Temperature       : Temperature in Celsius
- TempMin           : Minimum temperature in Celsius at the moment. This is minimal currently observed temperature (within large megalopolises and urban areas)
- TempMax           : Maximum temperature in Celsius at the moment. This is maximal currently observed temperature (within large megalopolises and urban areas)
- Pressure          : Atmospheric pressure
- Humidity          : Humidity, %
- SeaLevel          : Atmospheric pressure on the sea level
- GroundLevel       : Atmospheric pressure on the ground level
- Visibility        : Visibility, meter. The maximum value of the visibility is 10km
- WindSpeed         : Wind speed in meter/sec
- WindDirection     : Wind direction, degrees (meteorological)
- WindGust          : Wind gust in meter/sec
- Cloudiness        : Cloudiness, %
- RainVolumeLastHour: Rain volume for the last 1 hour, mm
- SnowVolumeLastHour: Snow volume for the last 1 hour, mm
- Sunrise           : Sunrise time in second from 00:00
- Sunset            : Sunset time in second from 00:00
- TimeRecord        : Time of data calculation in local UTC
- ConditionId       : Weather condition ID
- CityId            : City ID
- StationId         : Station ID

# Note
- Dont close cmd while API is running 
- API will run every 30 seconds, if you want to shutdown the API press crl+C in cmd
- You can see more detail about discription and get icon image [here](https://openweathermap.org/weather-conditions)
